brew bats-core
brew shellcheck
brew ctags
brew hadolint
brew glab
brew fx
brew openssh

directory "Creating directory for local sources"; do
    path "$HOME/.local/opt"
    parents true
done

directory "Creating directory for local logs"; do
    path "$HOME/.local/var/log"
    parents true
done

directory "Creating directory for local dump"; do
    path "$HOME/.local/var/debug"
    parents true
done

git "Clone yoot repository"; do
    repository https://gitlab.com/leonardmessier/shell/yoot.git
    destination "$HOME/.local/opt/yoot"
done

code "Making yoot binary executable"; do
    code "chmod +x $HOME/.local/opt/yoot/bin/yoot"
done

link "Symlinking yoot binary"; do
    target "$HOME/.local/opt/yoot/bin/yoot"
    to /usr/local/bin/yoot
    sudo true
done

directory "Creating base directory for logs"; do
    path "$HOME/.local/var/log"
    parents true
done

directory "Creating directory for cache"; do
    path "$HOME/.local/cache"
    parents true
done

file "Configure ssh-agent"; do
    path $HOME/.profile
    append $RECIPE_DIR/files/.profile
    only-if-missing "BEGIN ssh-agent"
done

git "Clone Traefik service configuration repository"; do
    repository https://gitlab.com/leonardmessier/services/traefik.git
    destination "$HOME/.local/opt/traefik"
done

template "Creating a runtime .env file"; do
    source "$HOME/.local/opt/traefik/.env.dist.esh"
    target "$HOME/.local/opt/traefik/.env"
    variables "main_network=${SHEF_RECIPE_DEV_MAIN['main_network']}"
done

directory "Create userland systemd directory"; do
    path "$HOME/.config/systemd/user/traefik"
    parents true
done

link "Symlinking traefik unit file to userland systemd configuration directory"; do
    target "$HOME/.local/opt/traefik/traefik.service"
    to "$HOME/.config/systemd/user/traefik.service"
    force true
done

code "Remove apache"; do
    code "apt-get remove apache2"
    sudo true
done

code "Enable traefik service"; do
    code "systemctl --user enable traefik.service"
done

git "Clone Traefik service configuration repository"; do
    repository https://gitlab.com/leonardmessier/services/maildev.git
    destination "$HOME/.local/opt/maildev"
done

template "Creating a runtime .env file"; do
    source "$HOME/.local/opt/maildev/.env.dist.esh"
    target "$HOME/.local/opt/maildev/.env"
    variables "main_network=${SHEF_RECIPE_DEV_MAIN['main_network']}"
done


link "Symlinking maildev unit file to userland systemd configuration directory"; do
    target "$HOME/.local/opt/maildev/maildev.service"
    to "$HOME/.config/systemd/user/maildev.service"
    force true
done

git "Download dnsmasq configuration"; do
    repository "https://gitlab.com/leonardmessier/config/dnsmasq.git"
    destination "$HOME/.local/etc/dnsmasq"
done

file "Copy dnsmasq to global etc directory"; do
    source "$HOME/.local/etc/dnsmasq/dnsmasq.conf"
    path "/etc/dnsmasq.conf"
    sudo true
    update true
done

apt dnsmasq

file "Configure asciidoctor"; do
    path $HOME/.local/.functions
    append $RECIPE_DIR/files/functions
    only-if-missing "BEGIN asciidoctor"
done

directory "Add directory for themes"; do
    path $HOME/.local/share/asciidoctor/pdf/themes
    parents true
done

download "Download go-task taskfile.dev binary"; do
    url "https://github.com/go-task/task/releases/download/v3.35.1/task_linux_amd64.tar.gz"
    path /tmp/task_linux_amd64.tar.gz
done

directory "Create opt directory for taskfile"; do
    path $HOME/.local/opt/taskfile
    parents true
done

code "Unpack it"; do
    code "tar xvzf /tmp/task_linux_amd64.tar.gz -C $HOME/.local/opt/taskfile"
done

link "Link the downloaded binary as taskfile since task is already used"; do
    target "$HOME/.local/opt/taskfile/task"
    to "$HOME/.local/bin/taskfile"
done