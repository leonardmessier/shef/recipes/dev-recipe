# BEGIN ssh-agent
SSH_AUTH_SOCK=~/.ssh/ssh-agent."$(hostname)".sock
export SSH_AUTH_SOCK
ssh-add -l 2>/dev/null >/dev/null
if [ $? -ge 2 ]; then
  eval "$(ssh-agent -s -a "$SSH_AUTH_SOCK" >/dev/null)"
fi
# END ssh-agent
